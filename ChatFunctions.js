{"use strict"};

let chatOnMessage = (message) => {
  console.log(message);
};

// Добавить обработчик события message для Чата Вебинара (webinarChat), который выводит в консоль сообщение 'Готовлюсь к ответу'. Обработчик создать в отдельной функции.
let readyToAnswer = () => {
  console.log("Готовлюсь к ответу");
};

// Для чата вконтакте (vkChat) добавить обработчик close, выводящий в консоль текст "Чат вконтакте закрылся :(".
let vkClose = () => {
  console.log("Чат вконтакте закрылся :(");
};

module.exports = {chatOnMessage, readyToAnswer, vkClose};
