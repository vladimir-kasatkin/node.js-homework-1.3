{"use strict"};

const cc = require('./ChatClasses.js');
const cf = require('./ChatFunctions.js');

let webinarChat =  new cc.ChatApp('webinar');
let facebookChat = new cc.ChatApp('=========facebook');
let vkChat =       new cc.ChatApp('---------vk');


webinarChat.on('message', cf.readyToAnswer);
webinarChat.on('message', cf.chatOnMessage);
facebookChat.on('message', cf.chatOnMessage);

// Для Вконтакте (vkChat) установить максимальное количество обработчиков событий, равное 2.
vkChat.setMaxListeners(2)
      .on('message', cf.readyToAnswer)
      .on('message', cf.chatOnMessage);

// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
  vkChat.removeListener('message', cf.chatOnMessage);
  vkChat.removeListener('message', cf.readyToAnswer);

  // Для чата вконтакте (vkChat) добавить обработчик close, выводящий в консоль текст "Чат вконтакте закрылся :(".
  vkChat.on('close', cf.vkClose);
  vkChat.close();
}, 10000 );

// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
facebookChat.removeListener('message', cf.chatOnMessage);
}, 15000 );

// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю вебинар');
  // Добавить код, который через 30 секунд отписывает chatOnMessage от вебинара webinarChat./
  webinarChat.removeListener('message', cf.chatOnMessage);
  webinarChat.removeListener('message', cf.readyToAnswer);
}, 20000 );
